# Programación declarativa y funcional
## Programación Declarativa. Orientaciones y pautas para el estudio (1998, 1999, 2001)
```plantuml
@startmindmap
* Programación\ndeclarativa 
 *_ ¿Que es?
  * Es un estilo\nun paradigma\nde programación
   *_ Surge
    * A raíz de problemas\nde la programación\nclásica
     *_ lenguajes
      * C
      * Pascal
     *_ Se basa
      * Modelo de von neumann
       *_ obliga a 
        * realizar una gestion\ndetallada de memoria
     *_ presenta
      * problemas de\nprogramación\nclasica
       *_ ya que
        * el programador\ntiene que dar\nmuchos detalles
         *_ es decir
          * se tiene que definir\npaso a paso
   *_ Se divide
    * programción\nfuncional
     *_ recurre a
      * lenguaje que utilizan\nlos matematicos 
       *_ Más especifico
        * Utiliza las funciones
         *_ las cuales 
          * Se asimilan a\nun programa
           *_ que contiene
            * datos de\nentrada
            * datos de\nsalida 
     *_ ventajas
      * No se realiza la gestion\nde memoria detallada
      * funciones de\norden superior
      * evalución peresosa
    * Programación\nlógica
     *_ recurre a
      * Lógica de predicados\nde primer orden
       *_ son
        * relaciones de\nentre objetos
         *_ este tipo\nde relación 
          * no establece un orden
           *_ de 
            * argumentos de\nentrada
            * argumentos de\nsalida
     *_ ventajas
      * permite ser mas\ndeclarativos
       *_ por ejemplo
        * permite definir\nuna relación\nfactorial
         *_ entre 
          * dos numeros
      * podemos preguntar\nal interprete
   *_ consiste en
    * dejar las tareas\nrutinarias al\nordenador
     *_ Especificamente
      * al compilador
       *_ por lo tanto
        * El programador puede dejar\nlas tareas en sus manos
   *_ ventajas 
    * se libera del control\ndel uso de memoria 
     *_ lo cual\npermite
      * utilizar otros recursos
       *_ los cuales
        * permiten llegar a\nun nivel mas alto
         *_ y a la vez 
          * se acerca al\nlenguaje humano
         *_ lo cual\nconsigue que
          * los programas\nsean mas cortos
          * mas faciles de realizar
          * mas faciles de depurar
   *_ proposito
    * reducir la complejidad\nde los programas
     *_ lo cual\nconlleva a
      * reducir el riesgo\nde cometer errores
       *_ y tambien a
        * reducir la farragosidad\ndel codigo
   *_ evolución
    * al principio\n al ordenador
     *_ se le
      * hablaba en\ncodigo maquina
       *_ es decir
        * en el lenguaje\npropio del ordenador
         *_ el cual consistie en
          * una programción
           *_ con
            * secuencias de ordenes
             *_ que eran
              * concretas
              * directas
              * muy sencillas
    * despues
     *_ se 
      * desarrollaron lenguajes\nde alto nivel
       *_ como
        * fortran
         *_ que
          * permitio separar un poco\ndel leguaje primitivo
           *_ permitio
            * escribir en codigo intermedio 
             *_ el cual
              * un compilador\ntraduce
               *_ a
                * una orden primitiva
       *_ sin embargo
        * estos lenguajes\nde alto nivel
         *_ no eran 
          * de tan alto nivel
           *_ el
            * programdor 
             *_ sigue
              * nesesitanto un\ncontrol detallado
               *_ de 
                * las secuencias\ny de la memoria

          
@endmindmap
```

## Lenguaje de Programación Funcional (2015)
```plantuml
@startmindmap
* Programación\nFuncional
 * paradigmas de\nprogramación
  *_ ¿que son?
   * son un modelo de computación
    *_ que 
     * los diferentes leguajes\ndotan de semantica
      *_ inicialmente
       * todos los lenguajes
        *_ estaban 
         * basados en\nel modelo
          *_ de
           * Von Neumann 
  *_ algunas varinates 
   * programación\norientada a objetos
    *_ la cual 
     * es pequeños\ntrozos 
      *_ de
       * codigo que interactuan\nentre si
        *_ que se
         * componen de instrucciones 
          *_ que se
           * ejecutan secuencialmente
   * programación logica
    *_ un programa
     * se forma mediante\nun conjunto 
      *_ de
       * sentencias 
        *_ que 
         * definen lo\nque es verdad
          *_ con
           * respecto a un problema
   * paradigma imperativo
    *_ un 
     * programa es una\nserie de instrucciones 
      *_ y 
       * una colección\nde datos
        *_ que 
         * se ejecuta en\nun orden adecuado
          *_ para
           * realizar un\ndeterminado computo
          *_ que se 
           * puede modificar
   * programación funcinal 
    *_ trabaja a 
     * base de funciones\nmatematicas 
      *_ tipos 
       * funciones constantes
       * funciones varibales 
      *_ donde\ncuenta
       * datos de entrada
       * datos de salida
      *_ permite 
       * hacer asignaciones
 * bases
  * lambda cálculo
   *_ se 
    * desarrollo en la\ndecada de 1930
     *_ por 
      * Church y Kleene
       *_ fue pensado
        * como un sistema 
        *_ para 
         * estudiar conceptos
         *_ pero
          * obtuvierón 
           *_ un 
            * sistema computaciónal\nbastante potente
   * logica combinatoria 
    *_ es
     * una variante 
      *_ que 
       * fueron los cimientos
        *_ para que
         * en el 1965 
          *_ permitio
           * modelar un lenguaje de programación


@endmindmap
```
